﻿CREATE DATABASE ejemploprogramacion4;
USE ejemploprogramacion4;

-- 1. Crea unn procedimiento simple que muestre por pantalla el texto
-- "esto es un ejemplo de procedimiento" (donde la cabecera de la 
-- columna sea "mensaje" 

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo1()
    BEGIN
      DECLARE mensaje varchar(100);
      SET mensaje = 'esto es un ejemplo de procedimiento';
      SELECT mensaje;
    END //
  DELIMITER ;

  CALL ejemplo1();

-- 2. Crear un procedimiento donde se declaren 3 variables internas, una de tipo 
-- entero con valor 1, otra de tipo varchar(10) con valor nulo por defecto y otra
-- de tipo decimal con 4 digitos y dos decimales con valor 10.48 por defecto.
  
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo2()
    BEGIN
      DECLARE vnumero int DEFAULT 1;
      DECLARE vtexto varchar(10) DEFAULT NULL;
      DECLARE vdecimal float(4,2) DEFAULT 10.48;

      SET vnumero = 13;
      SET vtexto = 'Racing';
      SET vdecimal = vnumero+vdecimal;

      SELECT vnumero, vtexto, vdecimal;
    END //
  DELIMITER ;  

  CALL ejemplo2();

-- 3. Copiar, comentar y corregir los errores en siguietne código 
   
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo3()
    BEGIN
      /* variables */
      DECLARE v_caracter1 char(1);
      DECLARE forma_pago enum('metálico','tarjeta','transferencia');
      /* cursores */
      /* control excepciones */
      /* programa */

      SET v_caracter1 = 'hola';
      -- SET forma_pago = 1;        No acepta este valor porque no esta definido en la enumeración
      -- SET forma_pago = 'cheque'; No acepta este valor porque no esta definido en la enumeración 
      -- SET forma_pago = 4;        No acepta este valor porque no esta definido en la enumeración
      SET forma_pago = 'TARJETA';
      SELECT forma_pago;
    END //
  DELIMITER ;

  CALL ejemplo3();

-- 4. Escribe un procedimiento que reciba un parametro de entrada (numero real) y 
-- muestre el numero en pantalla.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo4(numero int)
    BEGIN
      SELECT numero;
    END //
  DELIMITER ;

  CALL ejemplo4(7);

-- 5. Escribe un procedimiento que reciba un parametro de entrada y asigne ese parámetro
-- a una variable interna del mismo tipo. Después mostrar la variable en pantalla.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo5(numero int)
    BEGIN
      DECLARE vnumero int DEFAULT 0;
      SET vnumero = numero;
      SELECT vnumero;
    END //
  DELIMITER ;

  CALL ejemplo5(10);

-- 6. Escribe un procedimiento que reciba un número real de entrada y muestre un mensaje
-- indicando si el numero es positivo, negativo o cero,
  
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo6(numero float)
    BEGIN
      DECLARE vmensaje varchar(8) DEFAULT 'cero';

      IF (numero > 0) THEN
        SET vmensaje = 'positivo';
      ELSEIF (numero < 0) THEN
        SET vmensaje =  'negativo';
      END IF;

      SELECT CONCAT(numero,' ',vmensaje);
    END //
  DELIMITER ;

  CALL ejemplo6(16);
  CALL ejemplo6(-2);
  CALL ejemplo6(0);

-- 7. Escribe una función que reciba un número entero de entrada y devuelta TRUE si el 
-- numero es par o FALSE en caso contrario.
  
  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo7(numero int)
    RETURNS boolean
    BEGIN
      DECLARE vpar boolean DEFAULT FALSE;

      IF (numero % 2 = 0) THEN
        SET vpar = TRUE;
      END IF;

      RETURN vpar;
    END //
  DELIMITER ; 

  SELECT ejemplo7(25);
  SELECT ejemplo7(12);

-- 8. Escribe una funcion que devuelva el valor de la hipotenusa de un triangulo a partir 
-- de los valores de sus lados

  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo8(lado1 float, lado2 float)
    RETURNS float
    BEGIN
      DECLARE vhipotenusa float DEFAULT 0;

      SET vhipotenusa = SQRT(POW(lado1,2)+(POW(lado2,2)));

      RETURN vhipotenusa;
    END //
  DELIMITER ;

  SELECT ejemplo8(4,2);

-- 9. Modifique el procedimiento diseñado en el ejercicio anterior para que tenga un 
-- parametro de entrada, con el valor de un nuemro real, y un parametro de salida, con
-- una cadena de caracteres indicando si el numero es positivo, negativo o cero.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo9(numero float, OUT texto varchar(20))
    BEGIN  

      IF (numero > 0) THEN
        SET texto = 'positivo';
      ELSEIF (numero < 0) THEN
        SET texto = 'negativo';
      ELSE
        SET texto = 'cero';
      END IF;

      SET texto = CONCAT(numero,' ',texto);
    END //
  DELIMITER ;

  CALL ejemplo9(12,@texto);
  SELECT @texto;
  CALL ejemplo9(-5,@texto);
  SELECT @texto;
  CALL ejemplo9(0,@texto);
  SELECT @texto;

-- 10. Escribe un procedimiento que reciba un numero real de entrada, que representa
-- el valor de la nota de un alumno, y muestre el mensaje indicando que nota ha
-- obtenido teniendo en cuenta las siguientes condiciones:
-- [0,5) = Insuficiente
-- [5,6) = Aprobado
-- [6, 7) = Bien
-- [7, 9) = Notable
-- [9, 10] = Sobresaliente
-- En cualquier otro caso la nota no será válida.

  /* CON IF*/

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo10if(nota float)
    BEGIN

      DECLARE vtexto varchar(20) DEFAULT 'Nota no valida';
  
      IF (nota < 5) THEN
        SET vtexto = 'Insuficiente';
      ELSEIF (nota < 6) THEN
        SET vtexto = 'Aprobado';
      ELSEIF (nota < 7) THEN
        SET vtexto = 'Bien';
      ELSEIF (nota < 9) THEN
        SET vtexto = 'Notable';
      ELSEIF (nota BETWEEN 9 AND 10) THEN
        SET vtexto = 'Sobresaliente';
      END IF;

      SELECT vtexto;
      
    END //
  DELIMITER ;

  CALL ejemplo10if(7.3);

  /* CON CASE */
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo10case(nota float)
    BEGIN

      DECLARE vtexto varchar(20) DEFAULT '';
  
      CASE 
        WHEN nota < 5  THEN 
          SET vtexto = 'Insuficiente';
        WHEN nota < 6  THEN 
          SET vtexto = 'Aprobado';
        WHEN nota < 7  THEN 
          SET vtexto = 'Bien';
        WHEN nota < 9  THEN 
          SET vtexto = 'Notable';
        WHEN nota BETWEEN 9 AND 10 THEN 
          SET vtexto = 'Sobresaliente';
        ELSE 
          SET vtexto = 'Nota no Valida';
      END CASE;
      
      SELECT vtexto;
      
    END //
  DELIMITER ;

  CALL ejemplo10case(12);
  CALL ejemplo10case(4.9);
  CALL ejemplo10case(8.3);

-- 11.Escribe un procedimiento que me cree una tabla llamada notas en caso de que no
-- exista. El procedimiento recibirá dos argumentos de entrada que son nombre y nota
-- (numero real).

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo11(anombre varchar(50), anota float)
    BEGIN
      
      CREATE TABLE IF NOT EXISTS notas(
        id int AUTO_INCREMENT PRIMARY KEY,
        nombre varchar(50),
        nota float
      );

      INSERT INTO notas(nombre, nota)
        VALUE (anombre, anota);
      
    END //
  DELIMITER ;

  CALL ejemplo11('Juan',6.7);
  CALL ejemplo11('Luis',9.5);
  CALL ejemplo11('Juan',4.2);
  CALL ejemplo11('Sara',7.3);
  CALL ejemplo11('Angel',5.8);
  CALL ejemplo11('Juan',3.8);
  SELECT * F ROM notas n;

-- 12. Escribe una función que reciba como parámetro de entrada un valor numérico que
-- represente un día de la semana y que devuelva una cadena de caracteres con el nombre
-- del día de la semana correspondiente. Por ejemplo, para el valor de entrada 1 debería
-- devolver la cadena lunes.

  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo12(dia int)
    RETURNS varchar(10)
    BEGIN
      DECLARE vdia varchar(10) DEFAULT '';
      CASE 
        WHEN dia=1 THEN 
          SET vdia = 'Lunes';
        WHEN dia=2 THEN 
          SET vdia = 'Martes';
        WHEN dia=3 THEN 
          SET vdia = 'Miercoles';
        WHEN dia=4 THEN 
          SET vdia = 'Jueves';
        WHEN dia=5 THEN 
          SET vdia = 'Viernes';
        WHEN dia=6 THEN 
          SET vdia = 'Sabado';
        WHEN dia=7 THEN 
          SET vdia = 'Domingo';
        ELSE 
          SET vdia = 'NO VALIDO';
      END CASE;
      
      RETURN vdia;
    END //
  DELIMITER ;

  SELECT ejemplo12(5);
  SELECT ejemplo12(28);

-- 13. Crear un procedimiento almacenado que le pasemos un nombre de alumno y me
-- devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo sin 
-- utilizar cursores.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo13(anombre varchar(50))
    BEGIN
      DECLARE vnumAlumnos int DEFAULT 0;
      
      SET vnumAlumnos = (SELECT COUNT(*) FROM notas n WHERE n.nombre = anombre);

      SELECT vnumAlumnos;
    END //
  DELIMITER ;

  CALL ejemplo13('Juan');
  CALL ejemplo13('Luis');
  CALL ejemplo13('Sara');

-- 14. Crear un procedimiento almacenado que le pasemos un nombre de alumno y me 
-- devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo 
-- utilizando cursores (sin manejar excepciones). 

  /** faltan los cursores */
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo14(anombre varchar(50))
    BEGIN
      DECLARE vnumAlumnos int DEFAULT 0;
      
      SET vnumAlumnos = (SELECT COUNT(*) FROM notas n WHERE n.nombre = anombre);

      SELECT vnumAlumnos;
    END //
  DELIMITER ;

  CALL ejemplo14('Juan');
  CALL ejemplo14('Luis');
  CALL ejemplo14('Sara');

-- 15. Crear un procedimiento almacenado que le pasemos un nombre de alumno y me 
-- devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo 
-- utilizando cursores (manejando excepciones). 

  /** faltan los cursores y las excepciones*/
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo15(anombre varchar(50))
    BEGIN
      DECLARE vnumAlumnos int DEFAULT 0;
      
      SET vnumAlumnos = (SELECT COUNT(*) FROM notas n WHERE n.nombre = anombre);

      SELECT vnumAlumnos;
    END //
  DELIMITER ;

  CALL ejemplo15('Juan');
  CALL ejemplo15('Luis');
  CALL ejemplo15('Sara');

-- 16. Crear un procedimiento almacenado que me cree una tabla denominada usuarios 
-- solamente si no existe. 

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo16()
    BEGIN
      CREATE TABLE IF NOT EXISTS usuarios(
        id int AUTO_INCREMENT PRIMARY KEY,
        nombre varchar(20),
        contraseña varchar(20),
        nombreUsuario varchar(20)
      );
    END //
  DELIMITER ;

  CALL ejemplo16();

-- 17. Crear un procedimiento almacenado que me inserte datos en la tabla usuarios. 

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo17(nom varchar(20), cont varchar(20), usu varchar(20))
    BEGIN
      INSERT INTO usuarios (nombre, contraseña, nombreUsuario)
        VALUES (nom, cont, usu);
    END //
  DELIMITER ;

  CALL ejemplo17('Alex','1234','jandruco');
  SELECT * FROM usuarios u;

-- 18. Crear un procedimiento almacenado que llame a los procedimientos almacenados 
-- de los dos ejercicios anteriores. 

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo18(nom varchar(20), cont varchar(20), usu varchar(20))
    BEGIN
      CALL ejemplo16();
      CALL ejemplo17(nom,cont,usu);
    END //
  DELIMITER ;

  CALL ejemplo18('Alex','1234','jandruco');
  SELECT * FROM usuarios u;

-- 19. Crear una función que le pasemos un argumento de tipo texto (será el nombre
-- de usuario) y me devuelva verdadero en caso de que el usuario exista en la tabla
-- usuarios y FALSO en caso de que no. 

  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo19(texto varchar(20))
    RETURNS boolean
    BEGIN
      DECLARE vexiste boolean DEFAULT FALSE;
      DECLARE vcontador int DEFAULT 0;

      SET vcontador =(SELECT COUNT(*) FROM usuarios u WHERE u.nombre=texto);
      IF (vcontador <> 0) THEN
        SET vexiste = TRUE;
      END IF;
      RETURN vexiste;
    END //
  DELIMITER ;

  SELECT ejemplo19('Juan');
  SELECT ejemplo19('Alex');